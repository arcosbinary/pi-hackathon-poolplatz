package de.saxsys.sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        final Button btnClose = new Button("Push 2 close");
        btnClose.setOnAction(event -> System.exit(0));

        final StackPane parent = new StackPane(btnClose);

        final Scene scene = new Scene(parent, 800, 480);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
